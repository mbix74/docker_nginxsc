FROM debian:9 as build

ENV COMPILE_DIR /home/build
ENV V_LUA LuaJIT-2.0.5
ENV V_LUA_NGINX_MODULE 0.10.15
ENV V_NGINX nginx-1.19.3
ENV V_NGINX_DEVEL_KIT 0.3.1
ENV V_LUA_RESTRY_CORE 0.1.17
ENV V_LUA_RESTRY_LRU 0.11

RUN apt update \
&& apt install -y wget gcc make libpcre3-dev zlib1g-dev mc

RUN mkdir -p ${COMPILE_DIR}

RUN cd $COMPILE_DIR \
&& wget https://luajit.org/download/${V_LUA}.tar.gz \
&& tar xvfz ${V_LUA}.tar.gz \
&& cd ${V_LUA} \
&& make \
&& make install

RUN cd $COMPILE_DIR \
&& wget https://github.com/openresty/lua-nginx-module/archive/v${V_LUA_NGINX_MODULE}.tar.gz \
&& tar xvfz v${V_LUA_NGINX_MODULE}.tar.gz

RUN cd $COMPILE_DIR \
&& wget https://github.com/simplresty/ngx_devel_kit/archive/v${V_NGINX_DEVEL_KIT}.tar.gz \
&& tar xvfz v${V_NGINX_DEVEL_KIT}.tar.gz

RUN cd $COMPILE_DIR \
&& wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v${V_LUA_RESTRY_CORE}.tar.gz \
&& tar xvfz v${V_LUA_RESTRY_CORE}.tar.gz

RUN cd $COMPILE_DIR \
&& wget http://nginx.org/download/${V_NGINX}.tar.gz \
&& tar xvfz ${V_NGINX}.tar.gz \
&& cd ${V_NGINX} \
&& ./configure \
--sbin-path=/usr/sbin/nginx \
--conf-path=/etc/nginx/nginx.conf \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--lock-path=/var/lock/nginx.lock \
--pid-path=/run/nginx.pid \
--prefix=/opt/nginx \
--with-ld-opt="-Wl,-rpath,/usr/local/lib/libluajit-5.1.so.2" \
--add-module=$COMPILE_DIR/ngx_devel_kit-${V_NGINX_DEVEL_KIT} \
--add-module=$COMPILE_DIR/lua-nginx-module-${V_LUA_NGINX_MODULE} \
&& make \
&& make install

RUN cd $COMPILE_DIR \
&& wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v${V_LUA_RESTRY_CORE}.tar.gz \
&& tar xvfz v${V_LUA_RESTRY_CORE}.tar.gz \
&& cd lua-resty-core-${V_LUA_RESTRY_CORE} \
&& make install PREFIX=/opt/nginx

RUN cd $COMPILE_DIR \
&& wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v${V_LUA_RESTRY_LRU}.tar.gz \
&& tar xvfz v${V_LUA_RESTRY_LRU}.tar.gz \
&& cd lua-resty-lrucache-${V_LUA_RESTRY_LRU} \
&& make install PREFIX=/opt/nginx

FROM debian:9

ENV LD_LIBRARY_PATH=/usr/local/lib

COPY --from=build /usr/local/bin/luajit-2.0.5 /usr/local/bin
COPY --from=build /usr/local/lib/libluajit-5.1.so.2.0.5 /usr/local/lib
COPY --from=build /usr/local/lib/libluajit-5.1.a /usr/local/lib
COPY --from=build /opt/nginx/lib/lua/resty/core.lua /opt/nginx/lib/lua/resty/
RUN cd /usr/local/lib \
&& ln -s libluajit-5.1.so.2.0.5 libluajit-5.1.so.2 \
&& ln -s libluajit-5.1.so.2.0.5 libluajit-5.1.so
WORKDIR /
COPY --from=build /usr/sbin/nginx /usr/sbin/nginx
RUN mkdir -p /var/log/nginx /etc/nginx && touch /var/log/nginx/error.log && chmod +x /usr/sbin/nginx
COPY --from=build /etc/nginx/. /etc/nginx/
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
